<%-- 
    Document   : index
    Created on : 17-04-2020, 9:30:00
    Author     : franciscapaz.s
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap y CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <title>Evaluacion 01 - Francisca Paz Soto</title>
        <link href="Css/estilo.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container" id="cuadro2">
            <h1 id="titulo2">Resultado del Interes Simple</h1>
            <%
                float monto = (float) request.getAttribute("monto1");
                float tasa = (float) request.getAttribute("tasa1");
                float anos = (float) request.getAttribute("anos1");
                float interes = (float) request.getAttribute("interes");
                float interesmonto = (float) request.getAttribute("interesmonto");

                DecimalFormat formateador = new DecimalFormat("#.#");
            %>
            <h4>El calculo del interes simple corresponde a un monto de  <%= formateador.format(monto)%> , con una tasa anual del  <%= formateador.format(tasa)%>%, durante <%= formateador.format(anos)%> años</h4>
            <h4>Este le genera una tasa de interes simple del  <%= formateador.format(interes) %> anual </h4>
            <h4>La suma del monto + los intereses es de : <%= formateador.format(interesmonto) %></h4>
        </div>
    </body>
</html>
